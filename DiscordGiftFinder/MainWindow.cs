﻿using Leaf.xNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZertLib;

namespace DiscordGiftFinder
{
    public partial class MainWindow : MetroFramework.Forms.MetroForm
    {
        ProxyManager ProxyManager = new ProxyManager();

        static int hits, bad, retries = 0;
        static bool running = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void LoadProxiesBtn_Click(object sender, EventArgs e)
        {
            if (ProxyManager.GetAutoMode())
                ProxyManager.PullProxies();
            else if(proxiesOFD.ShowDialog() == DialogResult.OK)
                ProxyManager.LoadFromFile(proxiesOFD.FileName);
            loadProxiesBtn.Text = "Loaded: " + ProxyManager.Count();
        }

        private void AutoProxiesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (autoProxiesCheckbox.Checked)
                ProxyManager.EnterAutoMode();
            else 
                ProxyManager.ExitAutoMode();
        }

        private void ProxyTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (proxyTypeComboBox.SelectedIndex > 0)
                    switch (proxyTypeComboBox.SelectedIndex)
                    {
                        case 1:
                            ProxyManager.SetProxyType(ProxyType.HTTP);
                        if (ProxyManager.GetAutoMode())
                            ProxyManager.PullHTTP();
                            break;
                        case 2:
                            ProxyManager.SetProxyType(ProxyType.Socks4);
                        if (ProxyManager.GetAutoMode())
                            ProxyManager.PullSocks4();
                            break;
                        case 3:
                            ProxyManager.SetProxyType(ProxyType.Socks4A);
                            MetroFramework.MetroMessageBox.Show(this, "Auto Proxies not supported socks4a, sorry.", "Err", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case 4:
                            ProxyManager.SetProxyType(ProxyType.Socks5);
                        if (ProxyManager.GetAutoMode())
                            ProxyManager.PullSocks5();
                            break;
                    }
            else
                loadProxiesBtn.Enabled = false;
        }

        private void BotCountTrackbar_Scroll(object sender, ScrollEventArgs e)
        {
            botCountLabel.Text = "Bots: " + botCountTrackbar.Value;
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            if (running)
            {
                running = false;
                startBtn.Text = "Start";
            }
            else
            {
                running = true;
                Task.Run(() =>
                {
                    while (running)
                    {
                        ThreadSafeCalls.SetLabelText(hitsLabel, "Hits: " + hits.ToString());
                        ThreadSafeCalls.SetLabelText(badLabel, "Bad: " + bad.ToString());
                        ThreadSafeCalls.SetLabelText(retriesLabel, "Retries: " + retries.ToString());
                        Thread.Sleep(250);
                    }
                });
                for (int i = 0; i < botCountTrackbar.Value; i++)
                {
                    Task.Run(() => BotFunc());
                }
                startBtn.Text = "Stop";
            }
        }

        /// <summary>
        /// Generates a random string for use as discord gift code
        /// </summary>
        /// <returns></returns>
        private string genOne()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[16];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        private void BotFunc()
        {
            HttpRequest req = new HttpRequest();
            req.ManualMode = true;

            while (true)
            {
                req.Proxy = ProxyClient.Parse(ProxyManager.GetProxyType(), ProxyManager.GetProxyString());
                try
                {
                    var code = genOne();
                    var _resp = req.Get(string.Format("https://discordapp.com/api/v6/entitlements/gift-codes/{0}", code)).ToString();

                    if (_resp.Contains("Unknown Gift Code"))
                    {
                        bad++;
                    } else
                    {
                        hits++;
                        Console.WriteLine(_resp);
                        ThreadSafeCalls.AddRowToGrid(outputGrid, "https://discord.gift/" + code, "Nitro");
                    }
                }
                catch
                {
                    retries++;
                }
            }
        }
    }
}
