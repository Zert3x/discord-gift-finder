﻿namespace DiscordGiftFinder
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.loadProxiesBtn = new MetroFramework.Controls.MetroButton();
            this.proxyTypeComboBox = new MetroFramework.Controls.MetroComboBox();
            this.proxiesOFD = new System.Windows.Forms.OpenFileDialog();
            this.autoProxiesCheckbox = new MetroFramework.Controls.MetroCheckBox();
            this.outputGrid = new MetroFramework.Controls.MetroGrid();
            this.codeURL = new System.Windows.Forms.DataGridViewLinkColumn();
            this.codeType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.botCountTrackbar = new MetroFramework.Controls.MetroTrackBar();
            this.startBtn = new MetroFramework.Controls.MetroButton();
            this.botCountLabel = new MetroFramework.Controls.MetroLabel();
            this.hitsLabel = new MetroFramework.Controls.MetroLabel();
            this.badLabel = new MetroFramework.Controls.MetroLabel();
            this.retriesLabel = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.outputGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // loadProxiesBtn
            // 
            this.loadProxiesBtn.Location = new System.Drawing.Point(23, 63);
            this.loadProxiesBtn.Name = "loadProxiesBtn";
            this.loadProxiesBtn.Size = new System.Drawing.Size(146, 40);
            this.loadProxiesBtn.Style = MetroFramework.MetroColorStyle.Purple;
            this.loadProxiesBtn.TabIndex = 0;
            this.loadProxiesBtn.Text = "Load Proxies";
            this.loadProxiesBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.loadProxiesBtn.UseSelectable = true;
            this.loadProxiesBtn.Click += new System.EventHandler(this.LoadProxiesBtn_Click);
            // 
            // proxyTypeComboBox
            // 
            this.proxyTypeComboBox.FormattingEnabled = true;
            this.proxyTypeComboBox.ItemHeight = 23;
            this.proxyTypeComboBox.Items.AddRange(new object[] {
            "NONE",
            "HTTP",
            "SOCKS4",
            "SOCKS4a",
            "SOCKS5"});
            this.proxyTypeComboBox.Location = new System.Drawing.Point(23, 109);
            this.proxyTypeComboBox.Name = "proxyTypeComboBox";
            this.proxyTypeComboBox.PromptText = "Select Proxy Type";
            this.proxyTypeComboBox.Size = new System.Drawing.Size(146, 29);
            this.proxyTypeComboBox.Style = MetroFramework.MetroColorStyle.Purple;
            this.proxyTypeComboBox.TabIndex = 1;
            this.proxyTypeComboBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.proxyTypeComboBox.UseSelectable = true;
            this.proxyTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ProxyTypeComboBox_SelectedIndexChanged);
            // 
            // proxiesOFD
            // 
            this.proxiesOFD.FileName = "proxies.txt";
            this.proxiesOFD.Title = "Select Proxies File";
            // 
            // autoProxiesCheckbox
            // 
            this.autoProxiesCheckbox.AutoSize = true;
            this.autoProxiesCheckbox.Location = new System.Drawing.Point(176, 63);
            this.autoProxiesCheckbox.Name = "autoProxiesCheckbox";
            this.autoProxiesCheckbox.Size = new System.Drawing.Size(89, 15);
            this.autoProxiesCheckbox.Style = MetroFramework.MetroColorStyle.Purple;
            this.autoProxiesCheckbox.TabIndex = 2;
            this.autoProxiesCheckbox.Text = "Auto Proxies";
            this.autoProxiesCheckbox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.autoProxiesCheckbox.UseSelectable = true;
            this.autoProxiesCheckbox.CheckedChanged += new System.EventHandler(this.AutoProxiesCheckbox_CheckedChanged);
            // 
            // outputGrid
            // 
            this.outputGrid.AllowUserToResizeRows = false;
            this.outputGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.outputGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.outputGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.outputGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.outputGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.outputGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.outputGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codeURL,
            this.codeType});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.outputGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.outputGrid.EnableHeadersVisualStyles = false;
            this.outputGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.outputGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.outputGrid.Location = new System.Drawing.Point(271, 63);
            this.outputGrid.Name = "outputGrid";
            this.outputGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.outputGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.outputGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.outputGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.outputGrid.Size = new System.Drawing.Size(376, 273);
            this.outputGrid.Style = MetroFramework.MetroColorStyle.Purple;
            this.outputGrid.TabIndex = 3;
            this.outputGrid.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // codeURL
            // 
            this.codeURL.HeaderText = "Claim";
            this.codeURL.Name = "codeURL";
            // 
            // codeType
            // 
            this.codeType.HeaderText = "Gift Type";
            this.codeType.Name = "codeType";
            // 
            // botCountTrackbar
            // 
            this.botCountTrackbar.BackColor = System.Drawing.Color.Transparent;
            this.botCountTrackbar.Location = new System.Drawing.Point(23, 281);
            this.botCountTrackbar.Maximum = 200;
            this.botCountTrackbar.Minimum = 1;
            this.botCountTrackbar.Name = "botCountTrackbar";
            this.botCountTrackbar.Size = new System.Drawing.Size(242, 23);
            this.botCountTrackbar.Style = MetroFramework.MetroColorStyle.Purple;
            this.botCountTrackbar.TabIndex = 4;
            this.botCountTrackbar.Text = "Bots: 20";
            this.botCountTrackbar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.botCountTrackbar.Value = 20;
            this.botCountTrackbar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BotCountTrackbar_Scroll);
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(23, 310);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(146, 26);
            this.startBtn.Style = MetroFramework.MetroColorStyle.Purple;
            this.startBtn.TabIndex = 5;
            this.startBtn.Text = "Start";
            this.startBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.startBtn.UseSelectable = true;
            this.startBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // botCountLabel
            // 
            this.botCountLabel.AutoSize = true;
            this.botCountLabel.Location = new System.Drawing.Point(175, 313);
            this.botCountLabel.Name = "botCountLabel";
            this.botCountLabel.Size = new System.Drawing.Size(55, 19);
            this.botCountLabel.Style = MetroFramework.MetroColorStyle.Purple;
            this.botCountLabel.TabIndex = 6;
            this.botCountLabel.Text = "Bots: 20";
            this.botCountLabel.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // hitsLabel
            // 
            this.hitsLabel.AutoSize = true;
            this.hitsLabel.Location = new System.Drawing.Point(23, 141);
            this.hitsLabel.Name = "hitsLabel";
            this.hitsLabel.Size = new System.Drawing.Size(44, 19);
            this.hitsLabel.Style = MetroFramework.MetroColorStyle.Purple;
            this.hitsLabel.TabIndex = 7;
            this.hitsLabel.Text = "Hits: 0";
            this.hitsLabel.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // badLabel
            // 
            this.badLabel.AutoSize = true;
            this.badLabel.Location = new System.Drawing.Point(23, 160);
            this.badLabel.Name = "badLabel";
            this.badLabel.Size = new System.Drawing.Size(46, 19);
            this.badLabel.Style = MetroFramework.MetroColorStyle.Purple;
            this.badLabel.TabIndex = 8;
            this.badLabel.Text = "Bad: 0";
            this.badLabel.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // retriesLabel
            // 
            this.retriesLabel.AutoSize = true;
            this.retriesLabel.Location = new System.Drawing.Point(23, 179);
            this.retriesLabel.Name = "retriesLabel";
            this.retriesLabel.Size = new System.Drawing.Size(62, 19);
            this.retriesLabel.Style = MetroFramework.MetroColorStyle.Purple;
            this.retriesLabel.TabIndex = 9;
            this.retriesLabel.Text = "Retries: 0";
            this.retriesLabel.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 359);
            this.Controls.Add(this.retriesLabel);
            this.Controls.Add(this.badLabel);
            this.Controls.Add(this.hitsLabel);
            this.Controls.Add(this.botCountLabel);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.botCountTrackbar);
            this.Controls.Add(this.outputGrid);
            this.Controls.Add(this.autoProxiesCheckbox);
            this.Controls.Add(this.proxyTypeComboBox);
            this.Controls.Add(this.loadProxiesBtn);
            this.Name = "MainWindow";
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Discord Gift Finder";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.outputGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton loadProxiesBtn;
        private MetroFramework.Controls.MetroComboBox proxyTypeComboBox;
        private System.Windows.Forms.OpenFileDialog proxiesOFD;
        private MetroFramework.Controls.MetroCheckBox autoProxiesCheckbox;
        private MetroFramework.Controls.MetroGrid outputGrid;
        private System.Windows.Forms.DataGridViewLinkColumn codeURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeType;
        private MetroFramework.Controls.MetroTrackBar botCountTrackbar;
        private MetroFramework.Controls.MetroButton startBtn;
        private MetroFramework.Controls.MetroLabel botCountLabel;
        private MetroFramework.Controls.MetroLabel hitsLabel;
        private MetroFramework.Controls.MetroLabel badLabel;
        private MetroFramework.Controls.MetroLabel retriesLabel;
    }
}

