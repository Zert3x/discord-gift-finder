﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordGiftFinder
{
    class ThreadSafeCalls
    {
        delegate void SetLabelTextDelegate(MetroFramework.Controls.MetroLabel label, string text);
        delegate void SetLabelColorDelegate(MetroFramework.Controls.MetroLabel label, System.Drawing.Color color);
        delegate void SetTextboxTextDelegate(MetroFramework.Controls.MetroTextBox tb, String text);
        delegate void AppendGeneratedDelegate(MetroFramework.Controls.MetroTextBox textbox, Tuple<string, string> tup);
        delegate void AddRowToGridDelegate(MetroFramework.Controls.MetroGrid grid, string name, string stock);
        delegate void ClearGridDelegate(MetroFramework.Controls.MetroGrid grid);
        delegate void SetCellTextDelegate(MetroFramework.Controls.MetroGrid grid, int index, int stock);

        delegate void SetProgressIntegerDelegate(MetroFramework.Controls.MetroProgressBar bar, int val);

        public static void SetLabelText(MetroFramework.Controls.MetroLabel label, string text)
        {
            if (label.InvokeRequired)
            {
                var d = new SetLabelTextDelegate(SetLabelText);
                label.Invoke(d, new object[] { label, text });
            } else
            {
                label.Text = text;
            }
        }

        public static void SetLabelForeColor(MetroFramework.Controls.MetroLabel label, System.Drawing.Color color)
        {
            if (label.InvokeRequired)
            {
                var d = new SetLabelColorDelegate(SetLabelForeColor);
                label.Invoke(d, new object[] { label, color });
            } else
            {
                label.ForeColor = color;
            }
        }

        public static void SetTextboxText(MetroFramework.Controls.MetroTextBox textBox, string text)
        {
            if (textBox.InvokeRequired)
            {
                var d = new SetTextboxTextDelegate(SetTextboxText);
                textBox.Invoke(d, new object[] { textBox, text });
            }
            else
            {
                textBox.Text = text;
            }
        }

        public static void AppendGenerated(MetroFramework.Controls.MetroTextBox textbox, string combo)
        {
            if (textbox.InvokeRequired)
            {
                var d = new SetTextboxTextDelegate(AppendGenerated);
                textbox.Invoke(d, new object[] { textbox, combo });
            } else
            {
                textbox.Text += combo + "\r\n";
            }
        }

        public static void AddRowToGrid(MetroFramework.Controls.MetroGrid grid, string name, string stock)
        {
            if (grid.InvokeRequired)
            {
                var d = new AddRowToGridDelegate(AddRowToGrid);
                grid.Invoke(d, new object[] { grid, name, stock });
            } else
            {
                grid.Rows.Add(name, stock);
            }
        }

        public static void ClearGrid(MetroFramework.Controls.MetroGrid grid)
        {
            if (grid.InvokeRequired)
            {
                var d = new ClearGridDelegate(ClearGrid);
                grid.Invoke(d, new object[] { grid });
            } else
            {
                grid.Rows.Clear();
            }
        }

        public static void SetCellText(MetroFramework.Controls.MetroGrid grid, int index, int stock)
        {
            if (grid.InvokeRequired)
            {
                var d= new SetCellTextDelegate(SetCellText);
                grid.Invoke(d, new object [] { grid, index, stock });
            }
            else
            {
                grid.Rows[index].Cells[1].Value = (object)stock;
            }
        }
    }
}
